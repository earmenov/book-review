package com.book.review.app.sevice;

import com.book.review.app.model.Book;
import com.book.review.app.model.Review;
import com.book.review.app.model.payload.PageableReviews;

import java.io.IOException;
import java.util.List;

public interface ReviewScraperService {

    Book scrapeBook(String bookTitle) throws IOException;

    Book scrapeBookByAuthor(String bookTitle, String author) throws IOException;

    List<Review> scrapeTopReviews(String bookUrl) throws IOException;

    PageableReviews scrapePageableReviews(String allReviewsUrl, int pageNumber, int reviewNumber, String reviewType) throws IOException, InterruptedException;
}
