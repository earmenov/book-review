package com.book.review.app.model;

public class Review {

    public static class Builder {
        private final String content;
        private double rating;
        private String date;
        private String title;

        public Builder(String content) {
            this.content = content;
        }

        public Builder createdAt(String date) {
            this.date = date;
            return this;
        }

        public Builder title(String reviewTitle) {
            this.title = reviewTitle;
            return this;
        }

        public Builder rating(double rating) {
            this.rating = rating;
            return this;
        }

        public Review build() {
            Review review = new Review();
            review.content = this.content;
            review.date = this.date;
            review.title = this.title;
            review.rating = this.rating;
            return review;
        }
    }

    private String content;
    private double rating;
    private String date;
    private String title;

    private Review() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
