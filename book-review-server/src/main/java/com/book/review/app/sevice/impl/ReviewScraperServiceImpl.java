package com.book.review.app.sevice.impl;

import com.book.review.app.model.Book;
import com.book.review.app.model.Review;
import com.book.review.app.model.payload.PageableReviews;
import com.book.review.app.sevice.ReviewScraperService;
import info.debatty.java.stringsimilarity.NormalizedLevenshtein;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;

@Service
public class ReviewScraperServiceImpl implements ReviewScraperService {

    private static final String WEBSITE_URL = "https://www.goodreads.com/";
    private static final String AMAZON_URL = "https://www.amazon.com";
    private static final Set<String> stopWords = new HashSet<>();
    private static final NormalizedLevenshtein similarityService = new NormalizedLevenshtein();

    ReviewScraperServiceImpl() {
        loadStopWords();
    }

    @Override
    public Book scrapeBook(String bookTitle) throws IOException {
        return createBook(findBestBook(bookTitle.trim()));
    }

    @Override
    public Book scrapeBookByAuthor(String bookTitle, String author) throws IOException {
        return createBook(findBestBookByAuthor(bookTitle.trim(), author.trim()));
    }

    @Override
    public List<Review> scrapeTopReviews(String bookUrl) throws IOException {
        Document document = Jsoup.connect(WEBSITE_URL + bookUrl).get();
        if (document.getElementById("productTitle") != null) {
            document = Jsoup.connect(document.location()).get();
        } else {
            document = Jsoup.connect(AMAZON_URL + document.getElementsByClass("a-section a-spacing-medium")
                    .first().getElementsByClass("a-link-normal a-text-normal").first().attributes().get("href")).get();
        }

        Elements elements = document.select("div[class*=\"a-section review aok-relative\"]");
        List<Review> reviews = new ArrayList<>();
        for (Element e : elements) {
            if (e.getElementsByClass("review-image-tile-section").isEmpty()) {
                Iterator<TextNode> reviewParagraphs = e.getElementsByClass("a-expander-content reviewText review-text-content a-expander-partial-collapse-content")
                        .first().getElementsByTag("span").first().textNodes().iterator();

                String reviewContent = scrapeReviewContent(reviewParagraphs);
                if (!reviewContent.isEmpty()) {
                    reviews.add(buildReview(e, reviewContent));
                }
            }
        }

        return reviews;
    }

    @Override
    public PageableReviews scrapePageableReviews(String reviewsUrl, int pageNumber, int reviewNumber, String reviewType) throws IOException, InterruptedException {
        if (reviewNumber == 0) {
            Document document = Jsoup.connect(WEBSITE_URL + reviewsUrl).execute().parse();
            if (document.getElementById("productTitle") == null) {

                String bookPageUrl = AMAZON_URL + document.getElementsByClass("a-section a-spacing-medium")
                        .first().getElementsByClass("a-link-normal a-text-normal").first().attributes().get("href");

                document = Jsoup.connect(bookPageUrl).get();
            }
            reviewsUrl = AMAZON_URL + document
                    .getElementsByClass("a-link-emphasis a-text-bold").first().attributes().get("href");
        }

        Elements elements;
        List<Review> reviews = new ArrayList<>();

        do {
            Document document = Jsoup.connect(String.format("%s&filterByStar=%s&pageNumber=%d", reviewsUrl, reviewType, ++pageNumber)).get();
            elements = document.select("div[class*=\"a-section review aok-relative\"]");
            for (int i = reviewNumber % 10; i < elements.size(); i++, reviewNumber++) {
                if (elements.get(i).getElementsByClass("review-image-tile-section").isEmpty()) {
                    Iterator<TextNode> reviewParagraphs = elements.get(i).getElementsByClass("a-size-base review-text review-text-content")
                            .first().getElementsByTag("span").get(1).textNodes().iterator();
                    String reviewContent = scrapeReviewContent(reviewParagraphs);

                    if (!reviewContent.isEmpty()) {
                        reviews.add(buildReview(elements.get(i), reviewContent));
                        if (reviews.size() == 10) {
                            break;
                        }
                    }
                }
            }

        } while (reviews.size() < 10 && !elements.isEmpty());

        Collections.shuffle(reviews);
        PageableReviews pageableReviews = new PageableReviews();
        pageableReviews.setPageNumber(pageNumber);
        pageableReviews.setReviewNumber((reviewNumber % 10) + 1);
        pageableReviews.setHasMore(!elements.isEmpty());
        pageableReviews.setReviews(reviews);
        pageableReviews.setAllReviewsUrl(reviewsUrl);

        return pageableReviews;
    }

    private Book createBook(Element bestMatchBook) throws IOException {
        String bookHref = bestMatchBook.getElementsByClass("bookTitle").first().attributes().get("href");
        final Document document = Jsoup.connect(WEBSITE_URL + bookHref).get();

        Book book = new Book();
        book.setTitle(document.getElementById("bookTitle").text());
        book.setBookCoverSrc(document.getElementById("coverImage").attributes().get("src"));
        book.setAuthor(document.getElementsByClass("authorName").first().text());
        book.setRating(Double.parseDouble(document.select("span[itemprop = ratingValue]").text()));

        Elements e = document.getElementById("description").select("span[id*=\"freeText\"]");
        List<TextNode> description = document.getElementById("description").select("span[id*=\"freeText\"]").get(e.size() - 1).textNodes();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < description.size(); i++) {
            if ((i == 0 || i == description.size() - 1)
                    && (description.get(i).text().contains(".com") || description.get(i).text().contains("ISBN"))) {
                continue;
            }
            sb.append(description.get(i).text());
            if (i != description.size() - 1) {
                sb.append("<br/><br/>");
            }
        }
        book.setDescription(sb.toString());
        book.setBookReviewsUrl(document.getElementById("buyButton").attributes().get("href"));

        return book;
    }

    private Element findBestBook(String bookTitle) throws IOException {
        String bookSearchUrl = String.format(WEBSITE_URL + "search?q=%s&search_type=books", bookTitle);
        final Document document = Jsoup.connect(bookSearchUrl).get();

        try {
            return document.select("table").get(0).select("tr")
                    .stream()
                    .filter(book -> similarityService.similarity(bookTitle.toLowerCase(),
                            book.getElementsByClass("bookTitle").first().text().toLowerCase()) > 0.2)
                    .max(Comparator.comparing(el -> getBookRating(el.getElementsByClass("minirating").text())))
                    .orElseThrow(() -> {
                        throw new NoSuchElementException("Book with title \"%s\" was not found");
                    });
        } catch (IndexOutOfBoundsException e) {
            throw new NoSuchElementException("Book with title \"%s\" was not found");
        }
    }

    private Element findBestBookByAuthor(String bookTitle, String author) throws IOException {
        String bookSearchUrl = String.format(WEBSITE_URL + "search?q=%s&search_type=books", bookTitle);
        final Document document = Jsoup.connect(bookSearchUrl).get();

        try {
            return document.select("table").get(0).select("tr")
                    .stream()
                    .filter(el -> similarityService.similarity(bookTitle.toLowerCase(),
                            el.getElementsByClass("bookTitle").first().text().toLowerCase()) > 0.2)
                    .filter(book -> similarityService.similarity(book.getElementsByClass("authorName")
                            .first().text().toLowerCase(), author.toLowerCase()) > 0.75)
                    .max(Comparator.comparing(el -> getBookRating(el.getElementsByClass("minirating").text())))
                    .orElseThrow(() -> {
                        throw new NoSuchElementException("Book with title \"%s\" and author \"%s\" was not found");
                    });
        } catch (IndexOutOfBoundsException e) {
            throw new NoSuchElementException("Book with title \"%s\" and author \"%s\" was not found");
        }
    }

    private int getBookRating(String ratingWithNoise) {
        String[] words = ratingWithNoise.split(" ");
        return Integer.parseInt(words[words.length - 2].replace(",", ""));
    }

    private void loadStopWords() {
        try (BufferedReader reader = new BufferedReader(new FileReader("stopwords.txt"))) {
            String line = reader.readLine();
            while (line != null) {
                stopWords.add(line);
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String scrapeReviewContent(Iterator<TextNode> paragraphs) {
        StringBuilder sb = new StringBuilder();

        while (paragraphs.hasNext()) {
            TextNode node = paragraphs.next();
            if (hasTextStopWords(node.text())) {
                return "";
            }
            sb.append(node.text());
            if (paragraphs.hasNext()) {
                sb.append("<br/><br/>");
            }
        }
        return sb.toString();
    }

    private Review buildReview(Element reviewElement, String reviewContent) {
        String reviewTitle = reviewElement.select("a[class*=\"review-title\"]").text();
        if (reviewTitle.isEmpty()) {
            reviewTitle = reviewElement.select("span[class*=\"review-title\"]").text();
        }
        double commentRating = Double.parseDouble(reviewElement
                .getElementsByClass("a-icon-alt").text().split(" ")[0]);
        String date = reviewElement.getElementsByClass("a-size-base a-color-secondary review-date").text();
        return new Review
                .Builder(reviewContent)
                .title(reviewTitle)
                .rating(commentRating)
                .createdAt(date)
                .build();
    }

    private boolean hasTextStopWords(String text) {
        String[] words = text.split(" ");
        for (String word : words) {
            if (stopWords.contains(word.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
}
