package com.book.review.app;

import com.book.review.app.model.Review;
import com.book.review.app.model.payload.PageableReviews;
import com.book.review.app.sevice.ReviewScraperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/book")
@CrossOrigin(allowCredentials = "true", origins = "*")
public class ReviewScraperController {

    private final ReviewScraperService reviewScraperService;

    @Autowired
    ReviewScraperController(ReviewScraperService reviewScraperService) {
        this.reviewScraperService = reviewScraperService;
    }

    @GetMapping
    public ResponseEntity<?> getBook(@RequestParam String title, @RequestParam String author) {
        try {
            return ResponseEntity.ok((author.isEmpty())
                    ? reviewScraperService.scrapeBook(title)
                    : reviewScraperService.scrapeBookByAuthor(title, author));
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (NoSuchElementException e) {
            return (author.isEmpty())
                    ? ResponseEntity.badRequest().body(String.format(e.getMessage(), title))
                    : ResponseEntity.badRequest().body(String.format(e.getMessage(), title, author));
        }
    }

    @GetMapping(value = "/top-reviews")
    public ResponseEntity<List<Review>> getTopReviews(@RequestParam String url) {
        try {
            return ResponseEntity.ok(reviewScraperService.scrapeTopReviews(url));
        } catch (IOException | NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/reviews")
    public ResponseEntity<PageableReviews> loadReviews(
            @RequestHeader("Page-Number") int pageNumber,
            @RequestHeader("Review-Number") int reviewNumber,
            @RequestHeader("Reviews-Url") String reviewsUrl, @RequestParam String reviewsType) {
        try {
            return ResponseEntity
                    .ok(reviewScraperService.scrapePageableReviews(reviewsUrl, pageNumber, reviewNumber, reviewsType));
        } catch (IOException | NullPointerException | InterruptedException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
