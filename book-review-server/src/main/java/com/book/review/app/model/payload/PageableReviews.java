package com.book.review.app.model.payload;

import com.book.review.app.model.Review;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PageableReviews {

    private int pageNumber;
    private int reviewNumber;
    private boolean hasMore;
    private List<Review> reviews;
    private String allReviewsUrl;
    private String positiveReviewsUrl;

    public PageableReviews() {
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getReviewNumber() {
        return reviewNumber;
    }

    public void setReviewNumber(int reviewNumber) {
        this.reviewNumber = reviewNumber;
    }

    @JsonProperty
    public boolean getHasMore() {
        return hasMore;
    }

    @JsonIgnore
    public void setHasMore(boolean hasMore) {
        this.hasMore = hasMore;
    }

    @JsonProperty
    public List<Review> getReviews() {
        return reviews;
    }

    @JsonIgnore
    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    @JsonProperty
    public String getAllReviewsUrl() {
        return allReviewsUrl;
    }

    @JsonIgnore
    public void setAllReviewsUrl(String allReviewsUrl) {
        this.allReviewsUrl = allReviewsUrl;
    }

    @JsonProperty
    public String getPositiveReviewsUrl() {
        return positiveReviewsUrl;
    }

    @JsonIgnore
    public void setPositiveReviewsUrl(String positiveReviewsUrl) {
        this.positiveReviewsUrl = positiveReviewsUrl;
    }
}
