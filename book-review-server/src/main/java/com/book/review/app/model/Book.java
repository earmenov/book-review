package com.book.review.app.model;

public class Book {

    private String title;
    private String bookCoverSrc;
    private String author;
    private String description;
    private double rating;
    private String bookReviewsUrl;

    public Book() {}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBookCoverSrc() {
        return bookCoverSrc;
    }

    public void setBookCoverSrc(String bookCoverSrc) {
        this.bookCoverSrc = bookCoverSrc;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getBookReviewsUrl() {
        return bookReviewsUrl;
    }

    public void setBookReviewsUrl(String bookReviewsUrl) {
        this.bookReviewsUrl = bookReviewsUrl;
    }
}
