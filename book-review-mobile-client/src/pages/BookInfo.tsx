import React, { useState, useRef } from "react";
import { useHistory } from "react-router-dom";
import Book from "../components/Book";
import ReviewDisplayer from "../components/ReviewDisplayer";
import {
  IonPage,
  IonContent,
  IonFab,
  IonFabButton,
  IonHeader,
  IonToolbar,
  IonButtons,
  IonButton,
  IonIcon,
  IonTitle,
  IonGrid,
} from "@ionic/react";
import { chevronUp, arrowBack } from "ionicons/icons";
import "./TopPageScroller.css";

const BookInfo: React.FC = () => {
  const history = useHistory();

  const [showScrollTop, setShowScrollTop] = useState<boolean>(false);
  const reviewContentRef = useRef<HTMLIonContentElement>(null);

  const scrollToTop = () => {
    reviewContentRef.current?.scrollToTop(700);
  };

  const handleScroll = (event: any) => {
    if (
      !showScrollTop &&
      event.detail.scrollTop > 1100 &&
      event.detail.deltaY < -100
    ) {
      setShowScrollTop(true);
    } else if (
      (showScrollTop && event.detail.deltaY >= 20) ||
      event.detail.scrollTop <= 1100
    ) {
      setShowScrollTop(false);
    }
  };

  return (
    <IonPage>
      <IonContent
        ref={reviewContentRef}
        scrollEvents={true}
        onIonScroll={(event: any) => {
          handleScroll(event);
        }}
        style={{ "--background": "#404040" }}
      >
        <IonHeader>
          <IonToolbar color="primary">
            <IonButtons slot="start">
              <IonButton
                onClick={() => {
                  history.push("/home");
                }}
              >
                <IonIcon icon={arrowBack} slot="icon-only"></IonIcon>
              </IonButton>
              <IonTitle>Book Review</IonTitle>
            </IonButtons>
          </IonToolbar>
        </IonHeader>
        {showScrollTop && (
          <IonFab
            horizontal="center"
            vertical="top"
            slot="fixed"
            className="scrollTop"
          >
            <IonFabButton color="primary" onClick={scrollToTop}>
              <IonIcon icon={chevronUp}></IonIcon>
            </IonFabButton>
          </IonFab>
        )}
        <IonGrid>
          <Book />
          <ReviewDisplayer />
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default BookInfo;
