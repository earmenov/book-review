import React from "react";
import Backdrop from "@material-ui/core/Backdrop";
import "./Loading.css";

const Loading: React.FC<{ text: string; isLoading: boolean }> = (props) => {
  return (
    <Backdrop style={{ zIndex: 1000 }} open={props.isLoading}>
      <div className="container">
        <div className="book">
          <div className="pages"></div>
          <div className="pages"></div>
          <div className="pages"></div>
          <div className="pages"></div>
          <div className="pages"></div>
          <div className="pages"></div>
          <div className="pages"></div>
          <div className="pages"></div>
        </div>
        <div>
          <h1 className="title">{props.text}</h1>
        </div>
      </div>
    </Backdrop>
  );
};

export default Loading;
