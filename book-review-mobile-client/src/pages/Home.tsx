import React, { useState } from "react";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonIcon,
  IonButton,
  IonButtons,
  IonCol,
  IonRow,
  IonGrid,
} from "@ionic/react";
import { menu } from "ionicons/icons";
import BookLogo from "../components/BookLogo";
import BookSearchCard from "../components/BookSearchCard";
import Loading from "../pages/Loading";

const Home: React.FC = () => {
  const [loading, setLoading] = useState<boolean>(false);

  const handleBookSearchLoading = (isLoading: boolean) => {
    setLoading(isLoading);
  };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons>
            <IonButton>
              <IonIcon icon={menu} slot="icon-only"></IonIcon>
            </IonButton>
            <IonTitle>Book Review</IonTitle>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding" style={{ "--background": "#404040" }}>
        <IonGrid style={{ textAlign: "center", marginTop: "10%" }}>
          <IonRow className="ion-justify-content-center">
            <IonCol sizeLg="9" sizeXl="9">
              <BookLogo />
            </IonCol>
          </IonRow>
        </IonGrid>
        <BookSearchCard isBookSearchLoading={handleBookSearchLoading} />
        <Loading text="Searching for Book" isLoading={loading} />
      </IonContent>
    </IonPage>
  );
};

export default Home;
