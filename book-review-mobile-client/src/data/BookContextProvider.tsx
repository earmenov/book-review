import React, { useState } from "react";

import BookContext from "./book-context";

const BookContextProvider: React.FC = (props) => {
  const [bookCoverSrc, setBookCoverSrc] = useState<string>("");
  const [title, setTitle] = useState<string>("");
  const [author, setAuthor] = useState<string>("");
  const [description, setDescription] = useState<string>("");
  const [rating, setRating] = useState<number>(0);
  const [bookReviewsUrl, setBookReviewsUrl] = useState<string>("");

  const updateBookInfo = (book: any) => {
    setBookCoverSrc(book.bookCoverSrc);
    setTitle(book.title);
    setAuthor(book.author);
    setDescription(book.description);
    setRating(book.rating);
    setBookReviewsUrl(book.bookReviewsUrl);
  };

  return (
    <BookContext.Provider
      value={{
        bookCoverSrc,
        title,
        author,
        description,
        rating,
        bookReviewsUrl,
        updateBookInfo,
      }}
    >
      {props.children}
    </BookContext.Provider>
  );
};

export default BookContextProvider;
