import React from "react";

interface Book {
  bookCoverSrc: string;
  title: string;
  author: string;
  description: string;
  rating: number;
  bookReviewsUrl: string;
  updateBookInfo: (book: any) => void;
}

const BookContext = React.createContext<Book>({
  bookCoverSrc: "",
  title: "",
  author: "",
  description: "",
  rating: 0,
  bookReviewsUrl: "",
  updateBookInfo: () => {},
});

export default BookContext;
