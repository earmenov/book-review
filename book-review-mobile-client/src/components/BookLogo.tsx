import React from "react";

const BookLogo: React.FC = () => {
  return (
    <div>
      <svg height="0">
        <defs>
          <mask id="mask-b">
            <rect x="0" y="0" width="52" height="150" fill="#fff"></rect>
            <path
              d="M 14 14 h 20 a 3 3 0 0 1 3 3 v 6 a 3 3 0 0 1 -3 3 h -20 z"
              fill="#000"
            ></path>
            <path
              d="M 14 44 h 20 a 3 3 0 0 1 3 3 v 6 a 3 3 0 0 1 -3 3 h -20 z"
              fill="#000"
            ></path>
          </mask>
          <mask id="mask-o">
            <rect x="0" y="0" width="50" height="150" fill="#fff"></rect>
            <g transform="translate(25 40) scale(0.5 0.6)">
              <g transform="translate(-25 -40)">
                <path
                  fill="#000"
                  d="M 0 0 a 100 100 0 0 1 50 15 v 70 a 100 100 0 0 0 -50 -15 v -70"
                ></path>
              </g>
            </g>
          </mask>

          <symbol id="letter-o">
            <path
              transform="translate(0 21)"
              mask="url(#mask-o)"
              d="M 0 0 a 100 100 0 0 1 50 15 v 70 a 100 100 0 0 0 -50 -15 v -70"
            ></path>
            <path d="M 50 31 a 100 100 0 0 0 -45 -20 v 5 a 100 100 0 0 1 45 15"></path>
            <path d="M 50 26 a 100 100 0 0 0 -22 -26 v 10 a 100 100 0 0 1 22 16"></path>
          </symbol>
        </defs>
      </svg>

      <header>
        <svg viewBox="0 0 240 122" height="18vw" fill="rgb(200, 200, 200)">
          <g>
            <g transform="translate(0 21)">
              <path
                mask="url(#mask-b)"
                d="M 0 0 h 40 a 12 12 0 0 1 12 12 v 18 a 5 5 0 0 1 -5 5 a 5 5 0 0 1 5 5 v 18 a 12 12 0 0 1 -12 12 h -40"
              ></path>
            </g>
            <g transform="translate(64 0)">
              <use href="#letter-o"></use>
            </g>
            <g transform="translate(69 0)">
              <g transform="translate(50 0) scale(-1 1)">
                <g transform="translate(-50 0)">
                  <use href="#letter-o"></use>
                </g>
              </g>
            </g>
            <g transform="translate(181 21)">
              <path d="M 0 0 v 70 h 15 v -30 l 24 30 h 20 l -28 -35 l 28 -35 h -20 l -24 30 v -30 z"></path>
            </g>
          </g>
        </svg>
      </header>
      <div style={{ marginTop: "-1.8vw" }}>
        <label
          style={{
            fontFamily: "fantasy",
            fontSize: "13vw",
            letterSpacing: "1.2vw",
            color: "rgb(200, 200, 200)",
          }}
        >
          REVIEW
        </label>
      </div>
    </div>
  );
};

export default BookLogo;
