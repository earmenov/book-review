import React, {
  useState,
  forwardRef,
  useImperativeHandle,
  ForwardRefRenderFunction 
} from "react";
import { IonItem, IonInput, IonIcon, IonButtons, IonText } from "@ionic/react";

interface Handles {
  setInput: (value: string) => void;
  setInputError: (value: boolean) => void;
  getInput: () => string;
  getInputError: () => boolean;
}
interface Props {
  placeholder: string;
  inputErrorMsg: string;
  icon: any;
}

const BookSearchInput: ForwardRefRenderFunction <Handles, Props> = (
  props,
  ref
) => {
  const [input, setInput] = useState<string>("");
  const [inputError, setInputError] = useState<boolean>(false);

  useImperativeHandle(ref, () => ({
    setInput(value: string) {
      setInput(value);
    },
    getInput() {
      return input;
    },
    setInputError(value: boolean) {
      setInputError(value);
    },
    getInputError() {
      return inputError;
    },
  }));

  const handleInputChange = (value: string) => {
    if (inputError && value.length > 0) {
      setInputError(false);
    }

    setInput(value);
  };

  return (
    <React.Fragment>
      <IonItem
        lines="none"
        style={{
          "--background": "#2f2f2f",
          border: inputError
            ? "1px solid #e4cf81"
            : "1px solid rgb(255 255 255)",
          borderRadius: "12px",
          width: "92%",
        }}
      >
        <IonInput
          value={input}
          onIonChange={(e) => {
            handleInputChange(e.detail.value!);
          }}
          placeholder={props.placeholder}
          style={{
            color: inputError ? "#e4cf81" : "rgb(210, 210, 210)",
          }}
          clearInput
        />
        <IonButtons
          slot="start"
          style={{
            margin: "0px 14px 0px -2px",
          }}
        >
          <IonIcon
            icon={props.icon}
            slot="icon-only"
            style={{
              fontSize: "26px",
              color: inputError ? "#e4cf81" : "rgb(186, 186, 186)",
            }}
          ></IonIcon>
        </IonButtons>
      </IonItem>
      <IonText
        style={{
          textAlign: "left",
          display: "block",
          fontWeight: "bold",
          color: inputError ? "#e4cf81" : "rgb(90 90 90)",
          margin: inputError ? "12px 0 0 7%" : "0 0 -16px 0",
        }}
      >
        {props.inputErrorMsg}
      </IonText>
    </React.Fragment>
  );
};
export default forwardRef(BookSearchInput);
