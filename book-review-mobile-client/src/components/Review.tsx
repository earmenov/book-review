import React from "react";
import {
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonRow,
  IonCol,
  IonCardSubtitle,
  IonCardTitle,
} from "@ionic/react";
import Rating from "@material-ui/lab/Rating";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import Divider from "@material-ui/core/Divider";
import Parser from "html-react-parser";
import { withStyles } from "@material-ui/core/styles";
import "./styles/Review.css";

interface ReviewProps {
  title: string;
  rating: number;
  content: string;
  date: string;
}

const Review: React.FC<ReviewProps> = (props) => {
  return (
    <React.Fragment>
      <IonCard id="reviewCard">
        <IonCardHeader>
          <IonRow style={{ marginTop: "1%" }}>
            <IonCol>
              <StyledRating
                value={props.rating}
                precision={0.5}
                readOnly
                emptyIcon={<StarBorderIcon fontSize="inherit" />}
              />
            </IonCol>
          </IonRow>
          <IonCardTitle id="title">{props.title}</IonCardTitle>
        </IonCardHeader>
        <Divider id="divider"></Divider>
        <IonCardContent id="commentContent">
          <p
            id="comment"
            style={{
              height: props.content.length < 200 ? "100%" : "195px",
            }}
          >
            {Parser(props.content)}
          </p>
        </IonCardContent>
        <IonCardSubtitle id="reviewDate">{props.date}</IonCardSubtitle>
      </IonCard>
    </React.Fragment>
  );
};

const StyledRating = withStyles({
  iconEmpty: {
    color: "rgb(206, 206, 206)",
  },
})(Rating);

export default Review;
