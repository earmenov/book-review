import React, { useState, useContext, useRef } from "react";
import {
  useIonViewDidEnter,
  IonRow,
  IonCol,
  IonList,
  IonItem,
  IonInfiniteScroll,
  IonInfiniteScrollContent,
} from "@ionic/react";
import axios from "axios";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import RestoreIcon from "@material-ui/icons/Restore";
import FavoriteIcon from "@material-ui/icons/Favorite";
import SentimentVerySatisfiedIcon from "@material-ui/icons/SentimentVerySatisfied";
import MoodBadIcon from "@material-ui/icons/MoodBad";
import { withStyles } from "@material-ui/core/styles";
import Review from "./Review";
import Collapse from "@material-ui/core/Collapse";
import Loading from "../pages/Loading";
import BookContext from "../data/book-context";
import { isPlatform } from "@ionic/react";
import { HTTP } from "@ionic-native/http";
import RatingFilter from "./RatingFilter";
import Alerter from "../components/Alerter";
import "./styles/ReviewDisplayer.css";

const ReviewDisplayer: React.FC = () => {
  const bookCtx = useContext(BookContext);
  const alertRef = useRef<any>(null);

  const [starSelector, setStarSelector] = useState<number>(0);
  const [loading, setLoading] = useState<boolean>(false);
  const [navigationVal, setNavigationVal] = useState<number>(-1);
  const [reviews, setReviews] = useState<Array<Array<typeof Review>>>(
    Array(4).fill([])
  );

  const [pageableReviews, setPageableReviews] = useState(
    Array(3).fill({
      pageNumber: 0,
      reviewNumber: 0,
      hasMore: true,
      reviews: [],
      allReviewsUrl: "",
    })
  );

  useIonViewDidEnter(() => {
    setReviews(Array(4).fill([]));
    setNavigationVal(-1);
    setPageableReviews(
      Array(3).fill({
        pageNumber: 0,
        reviewNumber: 0,
        hasMore: true,
        reviews: [],
        allReviewsUrl: "",
      })
    );
  });

  const pickReviewType = (bottomNavVal: number) => {
    switch (bottomNavVal) {
      case 0:
        return "all_reviews";
      case 1:
        return "positive";
      case 2:
        return "critical";
    }
  };

  const handlePageableReviewsClick = (navVal: number) => {
    setLoading(true);
    isPlatform("android")
      ? HTTP.get(
          `http://192.168.1.144:8080/api/book/reviews?reviewsType=${pickReviewType(
            navVal
          )}`,
          {},
          {
            "Page-Number": pageableReviews[navVal].pageNumber.toString(),
            "Review-Number": pageableReviews[navVal].reviewNumber.toString(),
            "Reviews-Url":
              pageableReviews[navVal].allReviewsUrl === ""
                ? bookCtx.bookReviewsUrl
                : pageableReviews[navVal].allReviewsUrl,
          }
        )
          .then((result) => {
            const data = JSON.parse(result.data);
            setPageableReviews((prevState) => {
              prevState[navVal] = data;
              return [...prevState];
            });
            setLoading(false);
            setReviews((prevReviews) => {
              prevReviews[navVal] = [...prevReviews[navVal], ...data.reviews];
              return [...prevReviews];
            });
          })
          .catch(() => {
            handleError();
          })
      : axios
          .get(`/api/book/reviews?reviewsType=${pickReviewType(navVal)}`, {
            headers: {
              "Page-Number": pageableReviews[navVal].pageNumber,
              "Review-Number": pageableReviews[navVal].reviewNumber,
              "Reviews-Url":
                pageableReviews[navVal].allReviewsUrl === ""
                  ? bookCtx.bookReviewsUrl
                  : pageableReviews[navVal].allReviewsUrl,
            },
          })
          .then((result) => {
            setPageableReviews((prevState) => {
              prevState[navVal] = result.data;
              return [...prevState];
            });
            setLoading(false);
            setReviews((prevReviews) => {
              prevReviews[navVal] = [
                ...prevReviews[navVal],
                ...result.data.reviews,
              ];
              return [...prevReviews];
            });
          })
          .catch(() => {
            handleError();
          });
  };

  const handleTopReviewsClick = (newVal: number) => {
    if (reviews[newVal].length > 0) {
      setNavigationVal(-1);
    }
    setLoading(true);
    isPlatform("android")
      ? HTTP.get(
          `http://192.168.1.144:8080/api/book/top-reviews?url=${bookCtx.bookReviewsUrl}`,
          {},
          {}
        )
          .then((result: any) => {
            setLoading(false);
            setReviews((prevReviews) => {
              prevReviews[newVal] = JSON.parse(result.data);
              return [...prevReviews];
            });
            if (reviews[newVal].length > 0) {
              setNavigationVal(3);
            }
          })
          .catch(() => {
            handleError();
          })
      : axios
          .get(`/api/book/top-reviews?url=${bookCtx.bookReviewsUrl}`)
          .then((result: any) => {
            setLoading(false);
            setReviews((prevReviews) => {
              prevReviews[newVal] = [...prevReviews[newVal], ...result.data];
              return [...prevReviews];
            });
            if (reviews[newVal].length > 0) {
              setNavigationVal(newVal);
            }
          })
          .catch(() => {
            handleError();
          });
  };

  const handleError = () => {
    setLoading(false);
    alertRef.current.showAlert(true);
    setNavigationVal(-1);
  };

  const loadMoreReviews = (event: any) => {
    isPlatform("android")
      ? HTTP.get(
          `http://192.168.1.144:8080/api/book/reviews?reviewsType=${pickReviewType(
            navigationVal
          )}`,
          {},
          {
            "Page-Number": pageableReviews[navigationVal].pageNumber.toString(),
            "Review-Number": pageableReviews[
              navigationVal
            ].reviewNumber.toString(),
            "Reviews-Url": pageableReviews[navigationVal].allReviewsUrl,
          }
        )
          .then((result) => {
            const data = JSON.parse(result.data);
            setPageableReviews((prevState) => {
              prevState[navigationVal] = data;
              return [...prevState];
            });
            setReviews((prevReviews) => {
              prevReviews[navigationVal] = [
                ...prevReviews[navigationVal],
                ...data.reviews,
              ];
              return [...prevReviews];
            });
          })
          .then(() => {
            event.target.complete();
            !pageableReviews[navigationVal].hasMore &&
              (event.target.disabled = true);
          })
          .catch(() => {
            event.target.disabled = true;
            handleError();
          })
      : axios
          .get(
            `/api/book/reviews?reviewsType=${pickReviewType(navigationVal)}`,
            {
              headers: {
                "Page-Number": pageableReviews[navigationVal].pageNumber,
                "Review-Number": pageableReviews[navigationVal].reviewNumber,
                "Reviews-Url": pageableReviews[navigationVal].allReviewsUrl,
              },
            }
          )
          .then((result) => {
            setPageableReviews((prevState) => {
              prevState[navigationVal] = result.data;
              return [...prevState];
            });
            setReviews((prevReviews) => {
              prevReviews[navigationVal] = [
                ...prevReviews[navigationVal],
                ...result.data.reviews,
              ];
              return [...prevReviews];
            });
          })
          .then(() => {
            event.target.complete();
            !pageableReviews[navigationVal].hasMore &&
              (event.target.disabled = true);
          })
          .catch(() => {
            event.target.disabled = true;
            handleError();
          });
  };

  const handleBottomNavChange = (event: any, newValue: number) => {
    if (navigationVal !== newValue) {
      newValue !== 0 && setStarSelector(0);
      setNavigationVal(newValue);
      if (newValue >= 0 && newValue < 3 && reviews[newValue].length === 0) {
        handlePageableReviewsClick(newValue);
      } else if (newValue === 3 && reviews[newValue].length === 0) {
        handleTopReviewsClick(newValue);
      }
    } else {
      setNavigationVal(-1);
    }
  };

  const displayReviews = (reviewList: any) => {
    if (starSelector > 0 && navigationVal === 0) {
      reviewList = reviewList.filter(
        (review: any) => review.rating === starSelector
      );
    }

    return (
      <Collapse in={navigationVal !== -1} timeout={850} collapsedHeight={0}>
        <IonList id="reviewsList" lines="none">
          {reviewList.map((review: any, index: number) => {
            return (
              <IonRow className="ion-justify-content-center" key={index}>
                <IonCol sizeLg="10" sizeXl="9">
                  <IonItem style={{ "--background": "#404040" }}>
                    <Review
                      title={review.title}
                      rating={review.rating}
                      content={review.content}
                      date={review.date}
                    />
                  </IonItem>
                </IonCol>
              </IonRow>
            );
          })}
        </IonList>
      </Collapse>
    );
  };

  return (
    <React.Fragment>
      {loading && <Loading text="Searching for Reviews" isLoading={loading} />}
      {navigationVal >= 0 && navigationVal <= 2 && !loading && (
        <React.Fragment>
          {navigationVal === 0 && (
            <RatingFilter onFilterChange={setStarSelector} />
          )}
          {displayReviews(reviews[navigationVal])}
          {starSelector === 0 && (
            <IonInfiniteScroll
              id="infiniteScroller"
              threshold="100px"
              onIonInfinite={(event) => {
                loadMoreReviews(event);
              }}
            >
              <IonInfiniteScrollContent
                loadingSpinner="circles"
                loadingText="Loading more reviews"
              />
            </IonInfiniteScroll>
          )}
        </React.Fragment>
      )}
      {navigationVal === 3 && displayReviews(reviews[navigationVal])}
      <BottomNavigation
        id="bottomNav"
        value={navigationVal}
        showLabels
        onChange={handleBottomNavChange}
      >
        <StyledNavigationAction label="All Reviews" icon={<RestoreIcon />} />
        <StyledNavigationAction
          label="Positive"
          icon={<SentimentVerySatisfiedIcon />}
        />
        <StyledNavigationAction label="Critical" icon={<MoodBadIcon />} />
        <StyledNavigationAction label="Most Helpful" icon={<FavoriteIcon />} />
      </BottomNavigation>
      <Alerter
        alertHeader="Server Error"
        alertMessage="Something Went Wrong"
        ref={alertRef}
      />
    </React.Fragment>
  );
};

const StyledNavigationAction = withStyles({
  root: {
    color: "rgb(255 255 255 / 69%)",
    "&$selected": {
      color: "#e4cf81",
    },
  },
  label: {
    marginTop: "2px",
    fontSize: "0.7rem",
    "&$selected": {
      fontSize: "0.7rem",
    },
  },
  selected: {},
})(BottomNavigationAction);

export default ReviewDisplayer;
