import React, { useContext } from "react";
import {
  IonCard,
  IonCardHeader,
  IonCardContent,
  IonRow,
  IonCol,
  IonCardSubtitle,
  IonCardTitle,
} from "@ionic/react";
import Rating from "@material-ui/lab/Rating";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import Divider from "@material-ui/core/Divider";
import Parser from "html-react-parser";
import BookContext from "../data/book-context";
import { withStyles } from "@material-ui/core/styles";
import "./styles/Book.css";

const Book: React.FC = () => {
  const bookCtx = useContext(BookContext);

  return (
    <React.Fragment>
      <IonRow
        className="ion-justify-content-center"
        style={{ margin: "10px 0 70px 0" }}
      >
        <IonCol sizeMd="11" sizeLg="8" sizeXl="8">
          <IonCard id="bookCard">
            <img
              id="bookCover"
              src={bookCtx.bookCoverSrc}
              alt="book_thumbnail"
            />
            <IonCardHeader>
              <IonRow>
                <IonCol>
                  <StyledRating
                    value={bookCtx.rating}
                    precision={0.5}
                    readOnly
                    emptyIcon={<StarBorderIcon fontSize="inherit" />}
                  />
                </IonCol>
              </IonRow>
              <IonCardTitle id="bookTitle">{bookCtx.title}</IonCardTitle>
              <IonCardSubtitle id="subtitle">
                by {bookCtx.author}
              </IonCardSubtitle>
            </IonCardHeader>
            <Divider id="divider"></Divider>
            <IonCardContent id="descriptionContent">
              <p id="description">{Parser(bookCtx.description)}</p>
            </IonCardContent>
          </IonCard>
        </IonCol>
      </IonRow>
    </React.Fragment>
  );
};

const StyledRating = withStyles({
  iconEmpty: {
    color: "rgb(206, 206, 206)",
  },
})(Rating);

export default Book;
