import React, { useState } from "react";
import { IonRow, IonCol } from "@ionic/react";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import "./styles/SelectMenu.css";

interface Props {
  onFilterChange: (value: number) => void;
}

const RatingFilter: React.FC<Props> = (props) => {
  const [starSelector, setStarSelector] = useState<number>(0);
  return (
    <React.Fragment>
      <IonRow className="ion-justify-content-center">
        <IonCol style={{ textAlign: "center" }}>
          <FormControl variant="outlined" style={{ width: "45%" }}>
            <Select
              value={starSelector}
              onChange={(event: any) => {
                setStarSelector(event.target.value);
                props.onFilterChange(event.target.value);
              }}
            >
              <MenuItem value={0}>All Stars</MenuItem>
              <MenuItem value={5}>5 Stars</MenuItem>
              <MenuItem value={4}>4 Stars</MenuItem>
              <MenuItem value={3}>3 Stars</MenuItem>
              <MenuItem value={2}>2 Stars</MenuItem>
              <MenuItem value={1}>1 Star</MenuItem>
            </Select>
          </FormControl>
          <IonRow
            className="ion-justify-content-center"
            style={{ marginTop: "1%" }}
          >
            <IonCol>
              <label
                style={{
                  textAlign: "center",
                  color: "#d8e1e8",
                }}
              >
                Filtering by stars will not search for more reviews
              </label>
            </IonCol>
          </IonRow>
        </IonCol>
      </IonRow>
    </React.Fragment>
  );
};

export default RatingFilter;
