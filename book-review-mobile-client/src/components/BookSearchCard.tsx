import React, { useState, useRef, useContext } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import {
  IonGrid,
  IonCol,
  IonIcon,
  IonButton,
  IonCard,
  IonRow,
  IonLabel,
} from "@ionic/react";
import { HTTP } from "@ionic-native/http";
import { search, personCircleOutline, bookOutline } from "ionicons/icons";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import Collapse from "@material-ui/core/Collapse";
import BookContext from "../data/book-context";
import { isPlatform } from "@ionic/react";
import BookSearchInput from "./BookSearchInput";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Alerter from "../components/Alerter";

const BookSearchCard: React.FC<{
  isBookSearchLoading: (loading: boolean) => void;
}> = (props) => {
  const history = useHistory();
  const bookCtx = useContext(BookContext);

  const titleInputRef = useRef<any>(null);
  const authorInputRef = useRef<any>(null);
  const alertRef = useRef<any>(null);

  const [alertMessage, setAlertMessage] = useState<string>("");
  const [showAuthorField, setShowAuthorField] = useState<boolean>(false);

  const handleExpandClick = () => {
    setShowAuthorField(!showAuthorField);
    setTimeout(() => {
      authorInputRef.current?.setInput("");
      authorInputRef.current.setInputError(false);
    }, 400);
  };

  const handleBookSearch = () => {
    titleInputRef.current.getInput() === ""
      ? titleInputRef.current.setInputError(true)
      : titleInputRef.current.setInputError(false);

    showAuthorField && authorInputRef.current.getInput() === ""
      ? authorInputRef.current.setInputError(true)
      : authorInputRef.current.setInputError(false);

    if (
      titleInputRef.current.getInput() === "" ||
      (showAuthorField && authorInputRef.current.getInput() === "")
    ) {
      return;
    }

    props.isBookSearchLoading(true);
    isPlatform("android")
      ? HTTP.get(
          `http://192.168.1.144:8080/api/book?title=${titleInputRef.current.getInput()}&author=${authorInputRef.current.getInput()}`,
          {},
          {}
        )
          .then((result: any) => {
            props.isBookSearchLoading(false);
            bookCtx.updateBookInfo(JSON.parse(result.data));
          })
          .then(() => {
            titleInputRef.current?.setInput("");
            authorInputRef.current?.setInput("");
            history.push({
              pathname: "/book",
            });
          })
          .catch((error: any) => {
            setAlertMessage(error.error);
            props.isBookSearchLoading(false);
            alertRef.current.showAlert(true);
          })
      : axios
          .get(
            `/api/book?title=${titleInputRef.current.getInput()}&author=${authorInputRef.current.getInput()}`
          )
          .then((result: any) => {
            props.isBookSearchLoading(false);
            bookCtx.updateBookInfo(result.data);
          })
          .then(() => {
            titleInputRef.current?.setInput("");
            authorInputRef.current?.setInput("");
            history.push({
              pathname: "/book",
            });
          })
          .catch((error: any) => {
            setAlertMessage(error.response.data);
            props.isBookSearchLoading(false);
            alertRef.current.showAlert(true);
          });
  };

  return (
    <IonGrid style={{ textAlign: "-webkit-center" }}>
      <IonRow className="ion-justify-content-center">
        <IonCol sizeMd="11" sizeLg="9" sizeXl="9">
          <IonCard
            style={{
              boxShadow: "0px 2px 9px 0px rgba(0, 0, 0, 0.3)",
              margin: "3% 0 4% 0",
              backgroundColor: "rgb(90 90 90)",
            }}
          >
            <IonCol>
              <div style={{ marginTop: "4px" }}>
                <BookSearchInput
                  placeholder="Title"
                  inputErrorMsg="* A book title is required"
                  icon={bookOutline}
                  ref={titleInputRef}
                ></BookSearchInput>
              </div>
              <Collapse in={showAuthorField} timeout={600} collapsedHeight={0}>
                <div style={{ marginTop: "20px" }}>
                  <BookSearchInput
                    placeholder="Author"
                    inputErrorMsg="* An author is required"
                    icon={personCircleOutline}
                    ref={authorInputRef}
                  ></BookSearchInput>
                </div>
              </Collapse>
            </IonCol>
            <IonRow className="ion-justify-content-center">
              <IonCol sizeMd="8" sizeLg="5" sizeXl="5">
                <IonButton
                  style={{
                    width: "50%",
                    marginTop: "1%",
                    marginBottom: "4%",
                    height: "46px",
                    "--border-radius": "16px",
                  }}
                  onClick={handleBookSearch}
                >
                  <IonLabel
                    style={{ letterSpacing: "1px", fontWeight: "bold" }}
                  >
                    Search
                  </IonLabel>
                  <IonIcon icon={search} slot="start"></IonIcon>
                </IonButton>
              </IonCol>
            </IonRow>
            <Alerter
              alertHeader="Book not Found"
              alertMessage={alertMessage}
              ref={alertRef}
            />
          </IonCard>
        </IonCol>
      </IonRow>
      <IonCol>
        <FormControlLabel
          style={{ margin: "4px 16px 4px -11px" }}
          control={
            <CustomSwitch
              checked={showAuthorField}
              onChange={handleExpandClick}
            />
          }
          label={
            <Typography style={{ color: "rgb(200, 200, 200)" }}>
              Book Author
            </Typography>
          }
        />
      </IonCol>
    </IonGrid>
  );
};

const CustomSwitch = withStyles({
  switchBase: {
    color: "#555",
    "&$checked": {
      color: "#e6d38f",
    },
    "&$checked + $track": {
      backgroundColor: "#e6d38f",
    },
  },
  checked: {},
  track: {},
})(Switch);

export default BookSearchCard;
