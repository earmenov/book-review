import React, {
  useState,
  forwardRef,
  useImperativeHandle,
  ForwardRefRenderFunction,
} from "react";
import { IonAlert } from "@ionic/react";

interface ReviewProps {}

interface Handles {
  showAlert: (value: boolean) => void;
}
interface Props {
  alertHeader: string;
  alertMessage: string;
}

const Alert: ForwardRefRenderFunction<Handles, Props> = (props, ref) => {
  const [showSearchAlert, setShowSearchAlert] = useState<boolean>(false);

  useImperativeHandle(ref, () => ({
    showAlert(value: boolean) {
      setShowSearchAlert(value);
    },
  }));

  return (
    <IonAlert
      isOpen={showSearchAlert}
      onDidDismiss={() => setShowSearchAlert(false)}
      header={props.alertHeader}
      message={props.alertMessage}
      buttons={["OK"]}
    />
  );
};

export default forwardRef(Alert);
